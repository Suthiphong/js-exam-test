/*
    GLOBAL VARIBALE
*/
var userType = ['?', '?', '?'];
var correctNumber = [1, 2, 3];
var guessPosition = 0;
var retryCount = 4;

//

const userGuess = (number) => {
    if (userType.length < 3 || userType.includes('?')) {
        // userType = [...userType, number]

        // guessPosition 1 ? 
        userType[guessPosition] = number
        userNumberToPosition(number)
        guessPosition = userType.indexOf('?')
        console.log(userType)
        console.log(userType.indexOf('?'))
    }

    if (userType.length == 3 && !userType.includes('?')) {
        setTimeout(() => {
            console.log('guessProcess')
            guessProcess()
        }, 300)
    }
}

const helperNumberRender = () => {
    const helper = document.querySelector('.helper')
    const upperNumber = 10
    let html = '';
    for (let i = 0; i < upperNumber; i++) {
        html += `<div class="card-number" onClick="userGuess(${i})">
                    <label>${i}</label>
                 </div>`
    }

    helper.innerHTML = html
}

const userNumberToPosition = (number) => {
    const labelNumber = document.querySelectorAll('.number label')
    labelNumber[guessPosition].innerHTML = `${number}`
    // console.log(labelNumber[0].innerHTML)
}

const alertMessage = (status) => {
    const helper = document.querySelector('.helper')

    if (status) {
        // alert('You win.')
        // helper.innerHTML = `You win... !!! `
        helper.innerHTML = `You WIN <br><button onClick="location.reload();">RESTART</button>`

    } else {
        alert('incorrect try again')
    }
}


const guessProcess = () => {
    const labelNumber = document.querySelectorAll('.number label')
    if (userType.length == correctNumber.length) {
        userType.map((uT, idx) => {
            if (uT != correctNumber[idx]) {
                userType[idx] = '?'
            } else if (uT == correctNumber[idx]) {
                labelNumber[idx].style.color = 'lime';
            }

            // console.log(uT.toString().includes(correctNumber))
        })

        if (userType.includes('?')) {
            retry()
            alertMessage(false)
        } else {
            alertMessage(true)
        }

    }
}

const retry = () => {
    const labelNumber = document.querySelectorAll('.number label')
    const helper = document.querySelector('.helper')
    retryCount -= 1
    if (retryCount < 1) {
        alert('you lose.')
        helper.innerHTML = `You lose. Answer : ${correctNumber} <button onClick="location.reload();">RESTART</button>`
    }
    document.getElementById('count').innerText = retryCount
    labelNumber.forEach((v, idx) => {
        v.innerHTML = userType[idx]
    })

    guessPosition = userType.indexOf('?')
    console.log(guessPosition)
}

const restart = () => {
    const labelNumber = document.querySelectorAll('.number label')
    // const count = document.getElementById('count')
    labelNumber.forEach(v => {
        v.innerHTML = '?'
    })

    userType = ['?', '?', '?'];
}


const generateNumber = () => {
    const data = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]

    correctNumber = [...data].sort(() => Math.random() - 0.5).slice(0, 3)
    document.getElementById("answer").innerText = correctNumber
    console.log(correctNumber)
}

const init = () => {
    generateNumber()
    helperNumberRender()
}

const main = async () => {
    init()
}

main()

